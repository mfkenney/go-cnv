// Package cnv is used for parsing Seabird CNV data files
package cnv

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// Column describes a data variable in the file
type Column struct {
	// Zero-based column index
	Index int
	// Short name
	Name string
	// Description
	Desc string
	// Data units
	Units string
}

// Parsed CNV file representation
type Cnv struct {
	rdr      io.Reader
	format   []string
	info     []string
	metadata []string
	Columns  map[string]Column
	// Data collection start time
	T time.Time
	// Data values
	Data [][]float64
}

var columnPattern = `name (\d+) = ([a-zA-Z0-9_/]+): ([^\[]+)\[([^\]]+)\]`
var timeFormat = "Jan 2 2006 15:04:05"

// New parses a CNV file from an io.Reader and returns a new Cnv struct.
func New(r io.Reader, onlyheader bool) (*Cnv, error) {
	re, err := regexp.Compile(columnPattern)
	if err != nil {
		return nil, err
	}

	obj := Cnv{
		info:     make([]string, 0),
		metadata: make([]string, 0),
		Columns:  make(map[string]Column),
	}

	var (
		nrows, ncols int
	)

	format := make([]string, 0)
	scanner := bufio.NewScanner(r)
hdrloop:
	for scanner.Scan() {
		b := scanner.Bytes()
		if len(b) == 0 {
			continue
		}
		switch b[0] {
		case '*':
			line := string(b[1:])
			if strings.HasPrefix(line, "END*") {
				break hdrloop
			}
			obj.info = append(obj.info, line)
		case '#':
			line := string(b[1:])
			if strings.HasPrefix(line, " nquan = ") {
				ncols, err = strconv.Atoi(line[9:])
				if err != nil {
					return nil, err
				}
				obj.Data = make([][]float64, ncols)
			} else if strings.HasPrefix(line, " nvalues = ") {
				nrows, err = strconv.Atoi(line[11:])
				if err != nil {
					return nil, err
				}
				if obj.Data == nil {
					return nil, errors.New("Number of columns not set")
				}
				for i := 0; i < ncols; i++ {
					obj.Data[i] = make([]float64, 0, nrows)
				}
			} else if strings.HasPrefix(line, " start_time = ") {
				parts := strings.SplitN(line[14:], " ", 5)
				obj.T, err = time.Parse(timeFormat, strings.Join(parts[0:4], " "))
				if err != nil {
					return nil, err
				}
			} else if m := re.FindStringSubmatch(line); m != nil {
				idx, err := strconv.Atoi(m[1])
				if err != nil {
					return nil, err
				}
				col := Column{Index: idx, Name: m[2], Desc: m[3], Units: m[4]}
				obj.Columns[col.Name] = col
				format = append(format, "%f")
			}
			obj.metadata = append(obj.metadata, line)
		}
	}

	if scanner.Err() != nil {
		return nil, scanner.Err()
	}

	if !onlyheader {
		fmtstr := strings.Join(format, " ")
		row := make([]float64, len(format))
		irow := make([]interface{}, len(format))
		for i, _ := range row {
			irow[i] = &row[i]
		}

		for scanner.Scan() {
			_, err = fmt.Sscanf(scanner.Text(), fmtstr, irow...)
			if err == nil {
				for i, x := range row {
					obj.Data[i] = append(obj.Data[i], x)
				}
			}
		}

		if scanner.Err() != nil {
			return nil, scanner.Err()
		}
	} else {
		obj.rdr = r
		obj.format = format
	}

	return &obj, nil
}

// Metadata returns the original file metadata as a string
func (c *Cnv) Metadata() string {
	return strings.Join(c.metadata, "\r\n")
}

// Info returns the data collection information from the original file.
func (c *Cnv) Info() string {
	return strings.Join(c.info, "\r\n")
}

// ReadData reads the data into the Cnv struct (if New was called with onlyheader
// set to true).
func (c *Cnv) ReadData() error {
	if c.rdr != nil && c.format != nil {
		scanner := bufio.NewScanner(c.rdr)
		fmtstr := strings.Join(c.format, " ")
		row := make([]float64, len(c.format))
		irow := make([]interface{}, len(c.format))
		for i, _ := range row {
			irow[i] = &row[i]
		}

		for scanner.Scan() {
			_, err := fmt.Sscanf(scanner.Text(), fmtstr, irow...)
			if err == nil {
				for i, x := range row {
					c.Data[i] = append(c.Data[i], x)
				}
			}
		}

		return scanner.Err()
	}

	return nil
}
