package cnv

import (
	"math"
	"strings"
	"testing"
	"time"
)

const HEADER = `
# nquan = 6
# nvalues = 2810
# units = specified
# name 0 = depSF: Depth [salt water, ft], lat = 72.00
# name 1 = svCF: Sound Velocity [Chen-Millero, ft/s]
# name 2 = c0S/m: Conductivity [S/m]
# name 3 = sal00: Salinity, Practical [PSU]
# name 4 = t090F: Temperature [ITS-90, deg F]
# name 5 = flag:  0.000e+00
# span 0 =      0.653,   1621.226
# span 1 =    4589.37,    4790.08
# span 2 =   0.179199,   2.972072
# span 3 =     1.8281,    34.8728
# span 4 =    29.0458,    33.2462
# span 5 = 0.0000e+00, 0.0000e+00
# interval = seconds: 0.5
# start_time = Apr 15 2014 17:20:53 [Instrument's time stamp, header]
# bad_flag = -9.990e-29
*END*`

const DATA = `
      8.200    4704.43   2.353650    29.3100    29.0974  0.000e+00
      9.071    4704.39   2.355143    29.3426    29.0750  0.000e+00
      9.941    4704.58   2.358958    29.3994    29.0660  0.000e+00
     11.393    4704.78   2.362163    29.4446    29.0630  0.000e+00
     12.554    4704.97   2.365369    29.4898    29.0600  0.000e+00
     13.425    4705.16   2.368575    29.5351    29.0570  0.000e+00
     14.876    4705.43   2.372886    29.5950    29.0548  0.000e+00
     16.038    4705.50   2.374046    29.6119    29.0525  0.000e+00
     17.489    4705.49   2.373712    29.6083    29.0503  0.000e+00
     18.650    4705.59   2.374872    29.6227    29.0525  0.000e+00
     20.102    4705.69   2.376363    29.6441    29.0503  0.000e+00
     21.553    4705.69   2.376029    29.6393    29.0503  0.000e+00
     23.005    4705.79   2.377189    29.6549    29.0503  0.000e+00
     23.585    4705.84   2.377685    29.6603    29.0525  0.000e+00
     24.746    4705.89   2.378512    29.6727    29.0503  0.000e+00
     26.198    4705.91   2.378345    29.6701    29.0503  0.000e+00
`

func relerr(x, y float64) float64 {
	return math.Abs(y-x) / y
}

func TestHeader(t *testing.T) {
	cnv, err := New(strings.NewReader(HEADER), true)
	if err != nil {
		t.Fatal(err)
	}

	if len(cnv.Columns) != 5 {
		t.Errorf("Bad column count; expected 5, got %d", len(cnv.Columns))
	}

	if cnv.Columns["t090F"].Index != 4 {
		t.Errorf("Wrong name for column 4; expected t090F, got %q", cnv.Columns["t090F"].Name)
	}

	t0 := time.Date(2014, time.April, 15, 17, 20, 53, 0, time.UTC)
	if !cnv.T.Equal(t0) {
		t.Errorf("Wrong start time; expected %s, got %s", t0, cnv.T)
	}
}

func TestReadData(t *testing.T) {
	cnv, err := New(strings.NewReader(HEADER+DATA), false)
	if err != nil {
		t.Fatal(err)
	}

	if len(cnv.Data) != 6 {
		t.Errorf("Wrong data column count; expected 6, got %d", len(cnv.Data))
	}

	if len(cnv.Data[0]) != 16 {
		t.Errorf("Wrong data column count; expected 16, got %d", len(cnv.Data[0]))
	}

	if relerr(cnv.Data[0][0], 8.200) > 0.001 {
		t.Errorf("Bad value; expected 8.200, got %.3f", cnv.Data[0][0])
	}

	if relerr(cnv.Data[4][15], 29.0503) > 0.0001 {
		t.Errorf("Bad value; expected 29.0503, got %.4f", cnv.Data[4][15])
	}
}
